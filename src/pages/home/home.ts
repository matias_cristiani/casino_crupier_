import { Component } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';


import { Storage } from '@ionic/storage';
import { Insomnia } from '@ionic-native/insomnia';

import { LoginPage } from '../login/login';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
	public urlString: SafeResourceUrl;
	public loading: any;
  private counter: number;
constructor(public loadingCtrl: LoadingController, protected sanitizer: DomSanitizer, public navCtrl: NavController, private insomnia: Insomnia, private storage: Storage) {
	  this.counter = 0;
  }

  ionViewDidLoad(){
  	this.insomnia.keepAwake();
  }

  ionViewDidEnter(){
  	this.storage.get('urlCrupier').then((url) => {
  	   if (url) {
  	   	   console.log('home');
	       this.urlString = this.sanitizer.bypassSecurityTrustResourceUrl(url);
   	   }else{
   	   	console.log('ups');
  		this.navCtrl.push(LoginPage);
   	   }
  	}).catch((err)=>{
  		console.log('ups');
  		this.navCtrl.push(LoginPage);
  	})
  }

  tapEvent(){
    console.log('Tap Event',this.counter);
    this.counter = this.counter + 1;
    if (this.counter == 10) {
        this.counter = 0;
        this.storage.remove('urlCrupier');
        this.navCtrl.push(LoginPage);
    } 
  }
 
 

}
