import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';;

import { HomePage } from '../home/home';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  public stringVersion: string;
  public urlCrupier: string;
  
  constructor(public navCtrl: NavController, private storage: Storage, private alertCtrl: AlertController ) {
     this.urlCrupier = "http://";
     this.stringVersion = 'Versión 1.0 100';
  }
  
  ionViewDidLoad(){

  }

  ionViewDidEnter(){

    this.storage.get('urlCrupier').then((val) => {
    	if (val) {
	       console.log('Your url is', val);
	       this.navCtrl.push(HomePage);
        }
  	});

  }
  
  public submit(){

    //falta-url
    if(!this.urlCrupier || this.urlCrupier.length < 10){
      	console.log('A');
        let alert1 = this.alertCtrl.create({
		    title: 'ATENCIÓN',
		    // subTitle: '10% of battery remaining',
		    message: "La url ingresada no es valida para configurar en la aplicación.",
		    buttons: ['OK']
		  });
		
		return alert1.present();
    }
    
    //falta-http
    if (this.urlCrupier.indexOf('http') < 0) {
       	console.log('B');
          let alert2 = this.alertCtrl.create({
		    title: 'ATENCIÓN',
		    // subTitle: '10% of battery remaining',
		    message: "La url ingresada debe poseer un protocolo 'http://'' o 'https://'' .",
		    buttons: ['OK']
		  });
		
		return alert2.present();
    }
    console.log('C');
    let alert3 = this.alertCtrl.create({
	    title: 'CONFIRMAR URL',
	    subTitle: this.urlCrupier,
	    message: "Verifique la url que esta ingresando, ya que la misma no podra ser modificada posteriormente.",
	    buttons: [
	      {
	        text: 'Cancelar',
	        role: 'cancel',
	        handler: () => {
	          console.log('Cancel clicked');
	        }
	      },
	      {
	        text: 'OK',
	        handler: () => {
	          this.storage.set('urlCrupier', this.urlCrupier);
          	  this.navCtrl.push(HomePage);
	        }
	      }
	    ]
	  });

	  return alert3.present();
  }

}
